/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.entity;

import com.inspur.edp.cef.entity.entity.FieldType;
import com.inspur.edp.cef.spi.jsonser.abstractcefchange.BefDateSerUtil;
import com.inspur.edp.cef.spi.util.ExpressionUtil;
import io.swagger.models.auth.In;
import java.math.BigDecimal;
import java.sql.Date;

public class PropertyDefaultValue {
    private String value;
    private Object realValue;
    private DefaultValueType valueType = DefaultValueType.forValue(0);

    public PropertyDefaultValue(String value, Object realValue, DefaultValueType valueType) {
        this.value = value;
        this.realValue = realValue;
        this.valueType = valueType;
    }

    public final String getValue() {
        return value;
    }

    public final Object getRealValue() {
        if(valueType == DefaultValueType.Expression){
            return getRealValue(value, null, DefaultValueType.Expression);
        }
        return realValue;
    }

    public final DefaultValueType getValueType() {
        return valueType;
    }

    public static PropertyDefaultValue createDefaultValue(String defaultValue, FieldType fieldType,DefaultValueType defaultValueType)
    {
        Object rValue = getRealValue(defaultValue,fieldType,defaultValueType);
        return new PropertyDefaultValue(defaultValue,rValue,defaultValueType);
    }

    private static Object getRealValue(String defaultValue, FieldType fieldType,DefaultValueType defaultValueType) {
        if(defaultValueType==DefaultValueType.Expression){
            return ExpressionUtil.getExpressionValue(defaultValue);
        }
        if(defaultValue==null||defaultValue.equals(""))
            return defaultValue;
        switch (fieldType)
        {
            case String:
                return defaultValue;
            case Integer:
                return Integer.parseInt(defaultValue);
            case Decimal:
                return new BigDecimal(defaultValue);
            case Date:
                return Date.valueOf(defaultValue);
            case DateTime:
                return BefDateSerUtil.getInstance().readDateTime(defaultValue);
            case Boolean:
                return new Boolean(defaultValue);
            default:
                return defaultValue;
        }
    }
}
