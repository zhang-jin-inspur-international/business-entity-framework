/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.entity.info.propertyinfo;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.api.manager.serialize.CefSerializeContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.dynamicProp.DynamicPropSetImpl;
import com.inspur.edp.cef.entity.entity.dynamicProp.IDynamicPropSet;
import com.inspur.edp.cef.spi.common.ComponentInvokUtil;
import com.inspur.edp.cef.spi.jsonser.base.IDynamicPropSerItem;
import com.inspur.edp.cef.spi.jsonser.util.SerializerUtil;
import java.util.Objects;

public class DynamicSetPropertyInfo extends BasePropertyInfo {

  private IDynamicPropSerItem serComp;
  private String serCompId;

  public DynamicSetPropertyInfo(String serCompId) {
    Objects.requireNonNull(serCompId);
    this.serCompId = serCompId;
  }

  public DynamicSetPropertyInfo(IDynamicPropSerItem serComp) {
    Objects.requireNonNull(serComp);
    this.serComp = serComp;
  }

  private IDynamicPropSerItem getSerItem() {
    if (serComp != null) {
      return serComp;
    }
    serComp = (IDynamicPropSerItem) ComponentInvokUtil.getInstance(serCompId);
    return serComp;
  }

  @Override
  public void write(JsonGenerator writer, String propertyName, Object value,
      SerializerProvider serializer, CefSerializeContext serContext) {
    SerializerUtil
        .writeDynamicPropSetValue(writer, serializer, (IDynamicPropSet) value, propertyName,
            getSerItem());
  }

  @Override
  public void writeChange(JsonGenerator writer, String propertyName, Object value,
      SerializerProvider serializer, CefSerializeContext serContext) {
    SerializerUtil
        .writeDynamicPropSetChange(writer, serializer, (IChangeDetail) value, propertyName,
            getSerItem());
  }

  @Override
  public Object read(JsonParser reader, String propertyName, DeserializationContext serializer,
      CefSerializeContext serContext) {
    IDynamicPropSet result = new DynamicPropSetImpl();
    SerializerUtil.readDynamicPropSetValue(result, propertyName, getSerItem(), reader, serializer);
    return result;
  }

  @Override
  public Object read(JsonNode node, String propertyName, CefSerializeContext serContext) {
    return null;
  }

  @Override
  public Object readChange(JsonParser reader, String propertyName,
      DeserializationContext serializer, CefSerializeContext serContext) {
    return SerializerUtil.readDynamicPropSetChange(reader, serializer, propertyName, getSerItem());
  }

  @Override
  public Object readChange(JsonNode node, String propertyName, CefSerializeContext serContext) {
    return null;
  }

  @Override
  public void setValue(Object data, String propertyName, Object value, CefSerializeContext serContext) {

  }

  @Override
  public Object createValue() {
    return null;
  }

  @Override
  public String getPropValue(Object value) {
    if(value==null){
      return "";
    }
    return SerializerUtil.getDynamicPropValue((IDynamicPropSet)value);
  }


}
