/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.entity.info;

public  class EnumValueInfo {

	private  String displayValueKey;
	private  String enumCode;
	private  String defaultName;
	private  String stringIndex;
	private  Integer index;

	public EnumValueInfo()
	{

	}

	public EnumValueInfo(String displayValueKey,String enumCode,String defaultName,String stringIndex,Integer index)
	{
		this.displayValueKey = displayValueKey;
		this.enumCode = enumCode;
		this.defaultName = defaultName;
		this.stringIndex = stringIndex;
		this.index = index;
	}

	/**
	 * 枚举值资源项 e.g. (enumValue.I18nResourcePrefix).info
	 */
	public String getDisplayValueKey() {
		return displayValueKey;
	}

	/**
	 * 枚举值EnumValue e.g.enumValue.Value
	 */
	public String getEnumCode() {
		return enumCode;
	}

	/**
	 * 默认的枚举值显示值 e.g. enumValue.Name
	 */
	public String getDefaultName() {
		return defaultName;
	}

	/**
	 * 枚举值字符串索引 e.g. (enumValue.StringIndex)
	 */
	public String getStringIndex() {
		return stringIndex;
	}

	/**
	 * 枚举值整型索引 e.g. (enumValue.Index)
	 */
	public Integer getIndex() {
		return index;
	}
}
