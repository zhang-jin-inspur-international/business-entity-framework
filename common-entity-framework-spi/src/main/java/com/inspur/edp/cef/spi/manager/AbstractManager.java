/*package com.inspur.edp.cef.spi.manager;

import Inspur.Ecp.Caf.Common.*;
import Inspur.Gsp.Cef.entity.*;
import com.inspur.edp.cef.spi.mgraction.CefMgrAction;
import ICefManagerContext;

public abstract class AbstractManager
{
	private ICefManagerContext privateContext;
	public final ICefManagerContext getContext()
	{
		return privateContext;
	}
	public final void setContext(ICefManagerContext value)
	{
		privateContext = value;
	}

	protected AbstractManager()
	{
	}


		///#region Action
	protected final <TResult> TResult execute(CefMgrAction<TResult> action)
	{
		DataValidator.checkForNullReference(action, "action");

		action.setContext(super. getContext());

		return new MgrActionExecutor<TResult>(action, getContext()).execute();
	}

		///#endregion
}

 */