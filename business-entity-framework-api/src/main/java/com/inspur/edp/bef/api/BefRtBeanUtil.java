/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api;

import com.inspur.edp.bef.api.lcp.ILcpFactory;
import com.inspur.edp.bef.api.repository.IBefRepositoryFactory;
import com.inspur.edp.bef.api.services.IBefSessionManager;
import com.inspur.edp.cef.api.CefRtBeanUtil;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public class BefRtBeanUtil extends CefRtBeanUtil {

  private static volatile ILcpFactory lcpFactory;

  public static ILcpFactory getLcpFactory() {
    if (lcpFactory == null) {
      lcpFactory = SpringBeanUtils.getBean(ILcpFactory.class);
    }
    return lcpFactory;
  }

  private static volatile IBefRepositoryFactory befRepositoryFactory;

  public static IBefRepositoryFactory getBefRepositoryFactory() {
    if (befRepositoryFactory == null) {
      befRepositoryFactory = SpringBeanUtils.getBean(IBefRepositoryFactory.class);
    }
    return befRepositoryFactory;
  }

  private static volatile IBefSessionManager sessionManager;

  public static IBefSessionManager getBefSessionManager() {
    if (sessionManager == null) {
      sessionManager = SpringBeanUtils.getBean(IBefSessionManager.class);
    }
    return sessionManager;
  }
}
