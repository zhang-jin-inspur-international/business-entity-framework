/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.parameter.retrieve;

/** 数据检索时使用的数据来源类型 */
public enum RequestedBufferType {

  /**
   * 本次请求中的数据。
   *
   * <p>如果本次请求执行成功,提交到TransactionalBuffer,否则被丢弃
   */
  CurrentData(0),

  /** 包含会话过程中已经接受的变更数据 */
  TransactionalBuffer(1),

  /** 修改之前的老值,用于比较变更,获取差异或触发事件。会话过程中保持与数据库中一致 */
  Original(2);

  private int intValue;
  private static java.util.HashMap<Integer, RequestedBufferType> mappings;

  private static synchronized java.util.HashMap<Integer, RequestedBufferType> getMappings() {
    if (mappings == null) {
      mappings = new java.util.HashMap<Integer, RequestedBufferType>();
    }
    return mappings;
  }

  private RequestedBufferType(int value) {
    intValue = value;
    RequestedBufferType.getMappings().put(value, this);
  }

  public int getValue() {
    return intValue;
  }

  public static RequestedBufferType forValue(int value) {
    return getMappings().get(value);
  }
}
