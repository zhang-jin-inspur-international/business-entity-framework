/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.parameter.retrieve;

import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.condition.NodeSortInfo;
import com.inspur.edp.cef.entity.condition.RetrieveFilter;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

import java.util.HashMap;
import java.util.Map;

/**
 * 数据检索参数
 * <p>通过设置此类上的属性，可以在主表或从（从）表数据检索时，对检索行为进行一些控制。
 */
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class RetrieveParam {
  public RetrieveParam(){
    setCacheType(RequestedBufferType.CurrentData);
  }
  /**
   * 标识数据检索时是否加锁。 通常单纯的读取数据，不需要加锁，NeedLock设置为false；
   * 若检索后需对读取的数据进行修改，则需要加锁，将NeedLock设置为true。
   */
  private boolean needLock;

  public final boolean getNeedLock() {
    return needLock;
  }

  public final void setNeedLock(boolean value) {
    needLock = value;
  }

  /** 从哪个数据集中检索数据 */
  @Builder.Default
  private RequestedBufferType cacheType = RequestedBufferType.CurrentData;

  public final RequestedBufferType getCacheType() {
    return cacheType;
  }

  public final void setCacheType(RequestedBufferType value) {
    cacheType = value;
  }
  /** 传入的从表，从从表的排序条件 */
  private java.util.ArrayList<NodeSortInfo> nodeSortInfos;

  public final java.util.ArrayList<NodeSortInfo> getNodeSortInfos() {
    return nodeSortInfos;
  }

  public final void setNodeSortInfos(java.util.ArrayList<NodeSortInfo> value) {
    nodeSortInfos = value;
  }

  /** 是否强制从持久化取数 */
  private boolean forceFromRepository;

  public final boolean getForceFromRepository() {
    return forceFromRepository;
  }

  public final void setForceFromRepository(boolean value) {
    forceFromRepository = value;
  }

  /** 获取检索过滤条件， 通过过滤条件可以控制是否获取多语字段、仅获取部分子表数据等*/
  private RetrieveFilter retrieveFilter=new RetrieveFilter();

  public RetrieveFilter getRetrieveFilter() {
    return retrieveFilter;
  }

  public void setRetrieveFilter(RetrieveFilter retrieveFilter) {
    this.retrieveFilter = retrieveFilter;
  }
}
