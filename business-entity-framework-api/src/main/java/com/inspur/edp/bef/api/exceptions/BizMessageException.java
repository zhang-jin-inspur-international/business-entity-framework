/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.exceptions;

import com.inspur.edp.bef.entity.exception.BefFatalException;
import com.inspur.edp.bef.entity.exception.ExceptionLevel;
import com.inspur.edp.cef.api.message.IBizMessage;
import com.inspur.edp.cef.api.message.MessageLevel;

public class BizMessageException extends BefException {
  public BizMessageException(String code, IBizMessage message) {
    super(
        code,
        message.getMessageParams() != null
            ? String.format(message.getMessageFormat(), message.getMessageParams())
            : message.getMessageFormat(),
        null,
        ExceptionLevel.Error);
    if (message.getLevel().getValue() < MessageLevel.Error.getValue()) {
      throw new BefFatalException(
          new RuntimeException(/*"com.inspur.edp.bef.core.message", com.inspur.edp.bef.core.message.getLevel()*/ "message.Level必须是Error"));
    }
    setBizMessage(message);
  }
  //
  //  protected BizMessageException(SerializationInfo info, StreamingContext context) {
  //    super(info, context);
  //  }

  private IBizMessage privateMessage;

  public final IBizMessage getBizMessage() {
    return privateMessage;
  }

  public final void setBizMessage(IBizMessage msg) {
    privateMessage = msg;
  }
}
