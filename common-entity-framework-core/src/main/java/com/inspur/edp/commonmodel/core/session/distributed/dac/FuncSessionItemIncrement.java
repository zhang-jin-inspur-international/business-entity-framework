/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.commonmodel.core.session.distributed.dac;

import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.ValueObjModifyChangeDetail;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

//@JsonSerialize(using = SessionItemSerializer.class)
//@JsonDeserialize(using = SessionItemDeserializer.class)
public class FuncSessionItemIncrement {

    @Getter
    @Setter
    private int index;

    //TODO: 不是configId,应改名为AliasKey
    @Getter
    @Setter
    private String configId;

    @Getter
    @Setter
    private ValueObjModifyChangeDetail variable;

    @Getter
    @Setter
    private String varChangeString;

    @Getter
    @Setter
    private List<String> lockIds;

    @Getter
    @Setter
    private boolean reset;

    @Getter
    @Setter
    private List<IChangeDetail> changeDetails;

    @Getter
    @Setter
    private String changeDetailString;

    @Getter
    @Setter
    private Map<String, String> customs;
}
