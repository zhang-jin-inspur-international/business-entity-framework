/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.commonmodel.core.session.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.entity.changeset.Tuple;
import com.inspur.edp.commonmodel.core.session.distributed.dac.FuncSessionIncrement;

import java.io.IOException;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

public class ItemTypeDeserializer extends JsonDeserializer<Tuple<String,String>> {
    @Override
    public Tuple<String, String> deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String[] items = new String[2];
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(items, propName, jsonParser);
        }
        SerializerUtils.readEndObject(jsonParser);

        return new Tuple<>(items[0], items[1]);
    }

    private void readPropertyValue(String[] items, String propName, JsonParser jsonParser) {
        switch (propName) {
            case "item1":
                items[0] = SerializerUtils.readPropertyValue_String(jsonParser);
                break;
            case "item2":
                items[1] = SerializerUtils.readPropertyValue_String(jsonParser);
                break;
            default:
                throw new RuntimeException(String.format("未识别的属性名：%1$s", propName));
        }
    }

}
