/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.commonmodel.core.session.sessioncache;

import com.inspur.edp.commonmodel.core.session.tableentity.SessionCacheInfo;
import io.iec.edp.caf.data.orm.DataRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Date;
import java.util.List;

public interface SessionCacheInfoRepository extends DataRepository<SessionCacheInfo,String>, JpaSpecificationExecutor<SessionCacheInfo> {

    List<SessionCacheInfo> getSessionCacheInfosBySessionIdAndVersionGreaterThanOrderByVersion(String id, int ver);

    void deleteSessionCachesInfoBySessionIdAndVersionLessThanEqual(String id, int ver);

    void deleteAllBySessionId(String id);

    boolean existsSessionCacheInfoBySessionIdAndVersionGreaterThan(String id, int ver);

    List<SessionCacheInfo> findTop500ByCreatedOnAfterAndVersionEqualsOrderByCreatedOn(Date date, int ver);

    void deleteSessionCacheInfosBySessionIdIn(List<String> sessionIds);

}
