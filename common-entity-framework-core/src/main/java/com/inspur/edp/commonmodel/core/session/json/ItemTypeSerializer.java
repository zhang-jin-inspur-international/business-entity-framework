/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.commonmodel.core.session.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.entity.changeset.Tuple;

import java.io.IOException;

public class ItemTypeSerializer extends JsonSerializer<Tuple<String,String>> {
    @Override
    public void serialize(Tuple<String, String> value, JsonGenerator writer, SerializerProvider serializers){
        SerializerUtils.writeStartObject(writer);
        SerializerUtils.writePropertyValue(writer,"item1",value.getItem1());
        SerializerUtils.writePropertyValue(writer,"item2",value.getItem2());
        SerializerUtils.writeEndObject(writer);
    }
}
