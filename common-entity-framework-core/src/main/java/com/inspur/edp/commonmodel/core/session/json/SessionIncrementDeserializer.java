/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.commonmodel.core.session.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.entity.changeset.Tuple;
import com.inspur.edp.commonmodel.core.session.distributed.dac.FuncSessionIncrement;
import com.inspur.edp.commonmodel.core.session.distributed.dac.FuncSessionItemIncrement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.SneakyThrows;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

public class SessionIncrementDeserializer extends JsonDeserializer<FuncSessionIncrement> {

    @Override
    public FuncSessionIncrement deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        FuncSessionIncrement increment = new FuncSessionIncrement();
        ArrayList<Tuple<String,String>> itemTypes = new ArrayList<>();
        increment.setReset(false);
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(increment, propName, jsonParser, itemTypes);
        }
        SerializerUtils.readEndObject(jsonParser);
        return increment;
    }

    @SneakyThrows
    private void readPropertyValue(FuncSessionIncrement increment, String propName,
        JsonParser jsonParser, ArrayList<Tuple<String, String>> itemTypes) {
        switch (propName) {
            case "reset":
                increment.setReset(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case "itemTypes":
                readItemTypes(increment, jsonParser, itemTypes);
                break;
            case "item":
                readItem(increment,jsonParser, itemTypes);
                break;
            case "cust":
                increment.setCustoms(jsonParser.readValueAs(Map.class));
                jsonParser.nextToken();
                break;
            default:
                throw new RuntimeException(String.format("未识别的属性名：%1$s", propName));
        }
    }

    private void readItemTypes(FuncSessionIncrement increment, JsonParser jsonParser,
        ArrayList<Tuple<String, String>> itemTypes){
//        ArrayList<Tuple<String,String>> itemTypes = new ArrayList<>();
        ItemTypeDeserializer deserializer = new ItemTypeDeserializer();
        SerializerUtils.readArray(jsonParser,deserializer,itemTypes);
        increment.setItemTypes(itemTypes);
    }

    private void readItem(FuncSessionIncrement increment, JsonParser jsonParser,
        ArrayList<Tuple<String, String>> itemTypes){
        ArrayList<FuncSessionItemIncrement> increments = new ArrayList<>();
        SessionItemDeserializer deserializer = new SessionItemDeserializer(itemTypes);
        SerializerUtils.readArray(jsonParser,deserializer,increments);
        increment.setItem(increments);
    }
}
