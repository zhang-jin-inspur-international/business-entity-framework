/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.commonmodel.core.session.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.api.session.ICefSessionItem;
import com.inspur.edp.cef.entity.changeset.Tuple;
import com.inspur.edp.commonmodel.core.session.distributed.dac.FuncSessionIncrement;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.commonmodel.core.session.distributed.dac.FuncSessionItemIncrement;

import java.util.List;
import java.util.Map;
import org.springframework.util.StringUtils;

public class SessionIncrementSerializer extends JsonSerializer<FuncSessionIncrement> {

    private Map<String, ICefSessionItem> items;

    public SessionIncrementSerializer( Map<String, ICefSessionItem> items) {
        this.items = items;
    }

    @Override
    public void serialize(FuncSessionIncrement increment, JsonGenerator writer, SerializerProvider serializers)  {
        SerializerUtils.writeStartObject(writer);
        if(increment.isReset()){
            SerializerUtils.writePropertyValue(writer,"reset",increment.isReset());
        }
        if(increment.getItemTypes()!=null && increment.getItemTypes().size()>0) {
            writeItemTypes(writer, increment.getItemTypes());
        }
        if(increment.getItem()!=null && increment.getItem().size()>0) {
            writeItem(writer, increment);
        }
        if(increment.getCustoms() != null && !increment.getCustoms().isEmpty()) {
            SerializerUtils.writePropertyValue(writer,"cust",increment.getCustoms());
        }
        SerializerUtils.writeEndObject(writer);
    }

    private void writeItemTypes(JsonGenerator writer, List<Tuple<String,String>> itemTypes){
        SerializerUtils.writePropertyName(writer,"itemTypes");
        SerializerUtils.WriteStartArray(writer);
        for(Tuple<String,String> itemType : itemTypes){
            this.getItemTypeConvertor().serialize(itemType,writer,null);
        }
        SerializerUtils.WriteEndArray(writer);
    }

    private ItemTypeSerializer getItemTypeConvertor(){
        return new ItemTypeSerializer();
    }

    private void writeItem(JsonGenerator writer, FuncSessionIncrement increment) {
        SerializerUtils.writePropertyName(writer,"item");
        SerializerUtils.WriteStartArray(writer);
        for (FuncSessionItemIncrement itemIncrement : increment.getItem()) {
            this.getItemConvertor(increment,itemIncrement).serialize(itemIncrement,writer,null);
        }
        SerializerUtils.WriteEndArray(writer);
    }

    private SessionItemSerializer getItemConvertor(FuncSessionIncrement increment,FuncSessionItemIncrement itemIncrement){
        return new SessionItemSerializer(this.items.get(itemIncrement.getConfigId()),increment.getItemTypes());
    }
}
