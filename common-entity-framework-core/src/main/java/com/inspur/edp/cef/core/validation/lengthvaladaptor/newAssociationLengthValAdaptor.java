/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.validation.lengthvaladaptor;

import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.core.i18n.I18nResourceUtil;
import com.inspur.edp.cef.spi.entity.IAuthFieldValue;
import io.iec.edp.caf.boot.context.CAFContext;

public class newAssociationLengthValAdaptor extends LengthValAdaptor<IAuthFieldValue> {

  private int cefVersion;

  protected newAssociationLengthValAdaptor(String propName, int length, int prec) {
    super(propName, length, prec);
  }

  protected newAssociationLengthValAdaptor(String propName, int length, int prec,int cefVersion) {
    super(propName, length, prec);
    this.cefVersion = cefVersion;
  }

  @Override
  protected String getDisplayMessage(ICefValidationContext context) {
    String language= CAFContext.current.getLanguage();
    String exceptionCode= I18nResourceUtil
        .getResourceItemValue("pfcommon","cef_exception.properties","Gsp_Cef_AssoLen_0001");
    exceptionCode=String.format(exceptionCode,this.propName);
    return exceptionCode;
  }

  @Override
  protected boolean isValid(IAuthFieldValue value) {
    if(length == 0 || value == null){
      return true;
    }
    if(!(value instanceof IAuthFieldValue)){
      return false;
    }
    String AssociationValue = ((IAuthFieldValue) value).getValue();
    return AssociationValue == null || AssociationValue.length() <= length;
  }
}
