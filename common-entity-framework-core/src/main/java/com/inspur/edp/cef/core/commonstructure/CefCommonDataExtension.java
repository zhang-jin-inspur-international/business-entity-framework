/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.commonstructure;

import com.inspur.edp.caf.cef.rt.api.CommonData;
import com.inspur.edp.caf.cef.rt.spi.ObjectSerializerExtension;
import com.inspur.edp.caf.cef.rt.spi.PropertyAccessorExtension;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IEntityData;
import org.springframework.stereotype.Component;

public class CefCommonDataExtension implements PropertyAccessorExtension, ObjectSerializerExtension {

	private String commonDataType = "IEntityData";

	// region ObjectSerializerExtension
	@Override
	public String serialize(Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> T deSerialize(String s) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getSerializerType() {
		return commonDataType;
	}
	// endregion

	//region PropertyAccessorExtension
	@Override
	public Object getPropValue(CommonData commonData, String s) {

		return ((ICefData) commonData).getValue(s);
	}

	@Override
	public void setPropValue(CommonData commonData, String s, Object o) {
		((ICefData) commonData).setValue(s, o);
	}

	@Override
	public void addChild(CommonData commonData, String s, Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeChild(CommonData commonData, String s, Object o) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int getChildCount(CommonData commonData, String s) {
		return ((IEntityData) commonData).getChilds().get(s).size();
	}

	@Override
	public Object getChild(CommonData commonData, String s, int i) {
		//TODO: toArray
		return ((IEntityData) commonData).getChilds().get(s).toArray()[i];
	}

	@Override
	public String getAccessType() {
		return commonDataType;
	}
	// endregion
}

