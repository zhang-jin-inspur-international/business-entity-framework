/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.manager;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.inspur.edp.cef.api.dataType.base.IAccessorCreator;
import com.inspur.edp.cef.api.manager.ICefDataTypeManager;
import com.inspur.edp.cef.api.manager.ICefManagerContext;
import com.inspur.edp.cef.api.manager.serialize.CefSerializeContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefChangeJsonDeserializer;
import com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefChangeSerializer;
import com.inspur.edp.cef.spi.manager.MgrActionExecutor;
import com.inspur.edp.cef.spi.mgraction.CefMgrAction;
import java.io.IOException;

public abstract class CefDataTypeManager implements ICefDataTypeManager {

  public abstract ICefData createDataType();

  public abstract IAccessorCreator getAccessorCreator();


  ///#region Serializer
  public abstract String serialize(ICefData data);

  public abstract ICefData deserialize(String content);

  public final String serializeChange(IChangeDetail change) {
    try {
      return getChangeJsonMapper().writeValueAsString(change);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }

  public final IChangeDetail deserializeChange(String strChange) {
    try {
      return getChangeJsonMapper().readValue(strChange, IChangeDetail.class);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private ObjectMapper getChangeJsonMapper() {
    ObjectMapper mapper = new ObjectMapper();
    SimpleModule module = new SimpleModule();

    AbstractCefChangeSerializer serializer = getChangeSerializer();
    serializer.setCefSerializeContext(getSerizlizeContext());
    module.addSerializer(IChangeDetail.class, serializer);

    AbstractCefChangeJsonDeserializer deserializer = getChangeDeserializer();
    deserializer.setCefSerializeContext(getSerizlizeContext());
    module.addDeserializer(IChangeDetail.class, deserializer);

    mapper.registerModule(module);
    return mapper;
  }

  protected abstract AbstractCefChangeSerializer getChangeSerializer();

  protected abstract AbstractCefChangeJsonDeserializer getChangeDeserializer();

  protected abstract ICefManagerContext _CreateMgrContext();

  private ICefManagerContext context;

  public final ICefManagerContext getContext() {
    if (context == null) {
      context = _CreateMgrContext();
      context.setManager(this);
    }
    return context;
  }

  protected <TResult> TResult execute(CefMgrAction<TResult> action) {
    DataValidator.checkForNullReference(action, "action");

    MgrActionExecutor<TResult> tempVar = new MgrActionExecutor<TResult>();
    tempVar.setAction(action);
    tempVar.setContext(getContext());
    return tempVar.execute();
  }

  protected CefSerializeContext getSerizlizeContext() {
    return new CefSerializeContext();
  }
}
