/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core;

import com.inspur.edp.svc.reference.check.api.IReferenceChecker;
import io.iec.edp.caf.rpc.api.annotation.GspServiceBundle;
import io.iec.edp.caf.rpc.api.annotation.RpcService;
import io.iec.edp.caf.rpc.api.annotation.RpcServiceMethod;

@GspServiceBundle
@RpcService
public interface IBefReferenceChecker extends IReferenceChecker {
  @RpcServiceMethod(serviceId = "Inspur.Gsp.Cef.Core.BEDeleteService.CheckReference")
   boolean checkReference(String referer, String propertyName, String[] dataIds);
}
