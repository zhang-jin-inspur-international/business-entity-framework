/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.action;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class CompReflector<T> {

  private final Class[] parTypes;
  private final Class<T> t;
  private Constructor<T> ctor;

  public CompReflector(Class<T> t, Class... parTypes) {
    this.t = t;
    this.parTypes = parTypes;
  }

  public T createInstance(Object... pars) {
    if(length(parTypes) != length(pars)){
      throw new IllegalArgumentException();
    }
    if(ctor == null) {
      try {
        ctor = t.getDeclaredConstructor(parTypes);
      } catch (NoSuchMethodException e) {
        throw new RuntimeException(t.getTypeName() + "构件缺少构造函数");
      }
    }
    try {
      return ctor.newInstance(pars);
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
      throw new RuntimeException(t.getTypeName() + "创建构件失败", e);
    }
  }

  private static final int length(Object[] a){
    return a == null ? 0 : a.length;
  }
}
