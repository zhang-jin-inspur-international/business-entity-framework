/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//Java版临时屏蔽
//package Inspur.Gsp.Cef.Core;
//
//import Inspur.Ecp.Caf.Context.Service.*;
//import Inspur.Ecp.Caf.ServiceMgr.*;
//import Inspur.Gsp.Bef.Api.Repository.*;
//import Inspur.Gsp.Cef.Api.Repository.*;

//import Inspur.Gsp.Cef.entity.*;
//import Inspur.Gsp.Cef.entity.Condition.*;
//import Inspur.Gsp.Svc.ReferenceCheck.Api.*;
//
package com.inspur.edp.cef.core;


import com.inspur.edp.bef.api.repository.IBefRepositoryFactory;
import com.inspur.edp.cef.api.repository.IRootRepository;
import com.inspur.edp.svc.reference.check.api.IReferenceChecker;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.rpc.api.annotation.RpcServiceMethod;
import io.iec.edp.caf.runtime.config.CefBeanUtil;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.beans.BeanUtils;

public class BEDeleteService implements IBefReferenceChecker
{
	public BEDeleteService()
	{

	}
	public static String convertToJavaPackageName(String assemblyName) {
		if(!assemblyName.contains("."))
		{
			return assemblyName;
		}else {
			String[] list = assemblyName.split("\\.");
			String result = "";

			for (int i = 0; i < list.length - 1; i++) {
				String lowerCase = list[i].toLowerCase();
				if (i == 0) {
					if (lowerCase.equals("inspur")) {
						result = result.concat("com.inspur");
						continue;
					} else {
						result = result.concat(lowerCase);
						continue;
					}
				}
				result = result.concat(".");
				result = result.concat(lowerCase);
			}
			return result.concat(".") + list[list.length - 1];
		}
	}

	private void writeLog(String message)
	{
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS");
		Date date = new Date(System.currentTimeMillis());

		System.out.println(message+" at "+formatter.format(date));
	}

	@Override
	public final boolean checkReference(String referer, String propertyName, String[] dataIds)
	{
		writeLog("开始引用检查，Refer:"+referer+"，prpoertyName："+propertyName+"，dataid："+dataIds[0]);
		String[] str = propertyName.split("[/]", -1);
		String configId = convertToJavaPackageName(str[0]); //befConfigId
		String mainObjCode = str[1]; //主节点编号
		String nodeCode = str[2]; //节点编号
		String propName = str[3]; //属性名
		//IBefRepositoryFactory repoFactory= CefBeanUtil.getAppCtx().getBean(IBefRepositoryFactory.class);//;= ServiceManager.<IBefRepositoryFactory>GetService();
		IBefRepositoryFactory repoFactory= SpringBeanUtils.getBean(IBefRepositoryFactory.class);
		Object tempVar = repoFactory.createRepository(configId);
		IRootRepository br = (IRootRepository)((tempVar instanceof IRootRepository) ? tempVar : null);
		//TODO 年度表的外键检查删除检查
		String yearInfo = String.valueOf(CAFContext.current.getLoginTime().getYear());
		java.util.HashMap<String, Object> pars=new java.util.HashMap<String, Object>();
		pars.put("Fiscal",yearInfo);
		br.initParams(pars);
		try {
			boolean result = br.isRef(nodeCode, dataIds[0], propName);
			writeLog("结束引用检查，Refer:"+referer+"，prpoertyName："+propertyName+"，dataid："+dataIds[0]);
			return result;
		}
		catch (RuntimeException ex)
		{
			throw new RuntimeException("检查引用时发生错误，相关信息：referer："+referer+",propertyName:"+propertyName+"，错误消息："+ex.getMessage(),ex);
		}

	}
}
