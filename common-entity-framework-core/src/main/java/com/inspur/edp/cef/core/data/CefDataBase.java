/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.core.data.extendhandler.CefDataExtHandler;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IMultiLanguageData;
import com.inspur.edp.cef.entity.i18n.MultiLanguageInfo;
import com.inspur.edp.cef.spi.extend.datatype.ICefDataExtend;
import com.inspur.edp.cef.spi.extend.datatype.IDataExtendContainer;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public abstract class CefDataBase implements IMultiLanguageData, ICefData, IDataExtendContainer {

  @Override
  public final Object getValue(String propName) {
    RefObject<Object> result = new RefObject<>(null);
    if (extHandler != null && extHandler.getValue(propName, result)) {
      return result.argvalue;
    }
    return innerGetValue(propName);
  }

  protected abstract Object innerGetValue(String propName);

  @Override
  public final ICefData copySelf() {
    CefDataBase result = (CefDataBase) innerCopySelf();
    return result;
  }

  protected abstract ICefData innerCopySelf();

  @Override
  public final ICefData copy() {
    CefDataBase result = (CefDataBase) innerCopy();
    return result;
  }

  protected abstract ICefData innerCopy();

  @Override
  protected Object clone() throws CloneNotSupportedException {
    CefDataBase result = (CefDataBase)super.clone();
    result.extHandler = extHandler == null ? null : extHandler.copy();
    return result;
  }

  private List<String> propertyNames;
  @Override
  @JsonIgnore
  public final List<String> getPropertyNames() {
      if(propertyNames == null) {
      List<String> extNames = extHandler == null ? null : extHandler.getPropertyNames();
      if(extNames == null || extNames.isEmpty())
        propertyNames = innerGetPropertyNames();
      else
        propertyNames = new java.util.ArrayList<String>(innerGetPropertyNames()){{addAll(extNames);}};
    }
    return propertyNames;
  }

  protected abstract List<String> innerGetPropertyNames();

  @Override
  public final void setValue(String propName, Object value) {

    if (extHandler != null && extHandler.setValue(propName, value)) {
      return;
    }
    innerSetValue(propName, value);
  }

  protected abstract void innerSetValue(String propName, Object value);

  //region
  private CefDataExtHandler extHandler;

  protected CefDataExtHandler getExtHandler() {
    if (extHandler == null) {
      extHandler = createExtHandler();
      Objects.requireNonNull(extHandler, "createExtHandler returns null");
    }
    return extHandler;
  }

  protected abstract CefDataExtHandler createExtHandler();

  public void addExtend(ICefDataExtend... ext) {
    Objects.requireNonNull(ext, "ext");

    Arrays.stream(ext).forEach(item -> getExtHandler().addExtend(item));
  }

  @Override
  @JsonIgnore
  public List<ICefDataExtend> getExtends() {
    if (extHandler == null) {
      return Collections.emptyList();
    }
    return extHandler.getExtendList();
  }
  //endregion

  // region multiLanguage
  @JsonIgnore
  private Map<String, MultiLanguageInfo> multiLanguageInfos;
  @JsonIgnore
  @Deprecated
  public Map<String, MultiLanguageInfo> getMultiLanguageInfos(){
    if(this.multiLanguageInfos==null){
      this.multiLanguageInfos=new HashMap<>();
    }
    return multiLanguageInfos;
  }
  // endregion
}
