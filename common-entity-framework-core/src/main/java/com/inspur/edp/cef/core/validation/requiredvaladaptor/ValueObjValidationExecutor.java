/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.validation.requiredvaladaptor;

import com.inspur.edp.cef.api.dataType.valueObj.ICefValueObjContext;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.ValueObjModifyChangeDetail;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.spi.validation.IValidation;
import com.inspur.edp.cef.spi.validation.IValueObjRTValidationAssembler;

public class ValueObjValidationExecutor
{
	private ICefValueObjContext privateValueObjContext;
	private ICefValueObjContext getValueObjContext()
	{
		return privateValueObjContext;
	}
	private void setValueObjContext(ICefValueObjContext value)
	{
		privateValueObjContext = value;
	}

	private IValueObjRTValidationAssembler privateAssembler;
	protected final IValueObjRTValidationAssembler getAssembler()
	{
		return privateAssembler;
	}

	private IChangeDetail privateChange;
	private IChangeDetail getChange()
	{
		return privateChange;
	}
	private void setChange(IChangeDetail value)
	{
		privateChange = value;
	}

	private ICefValidationContext privateContext;
	private ICefValidationContext getContext()
	{
		return privateContext;
	}
	private void setContext(ICefValidationContext value)
	{
		privateContext = value;
	}

	public ValueObjValidationExecutor(ICefValueObjContext valueObjContext, IValueObjRTValidationAssembler assembler, ValueObjModifyChangeDetail changedetail)
	{
		DataValidator.checkForNullReference(valueObjContext, "valueObjContext");
		DataValidator.checkForNullReference(assembler, "assembler");

		setValueObjContext(valueObjContext);
		privateAssembler = assembler;
		setChange(changedetail);
	}

	public final void execute()
	{
		setContext(getDeterminationContext());
		if (getContext() == null)
		{
			throw new UnsupportedOperationException("getDeterminationContext() returning null");
		}
		//assembler.Data = Data;

		executeBelongingDeterminations();

		executeDeterminations();
	}

	private void executeBelongingDeterminations()
	{
		java.util.List<IValidation> validations = getAssembler().getBelongingValidations();
		if (validations == null || validations.isEmpty())
		{
			return;
		}

		executeDeterminationsCore(getChange(), validations);
	}

	private void executeDeterminations()
	{
		java.util.List<IValidation> validations = getAssembler().getValidations();
		if (validations == null || validations.isEmpty())
		{
			return;
		}

		executeDeterminationsCore(getChange(), validations);
	}

	private void executeDeterminationsCore(IChangeDetail change, Iterable<IValidation> items)
	{
		for (IValidation item : items)
		{
			if (item.canExecute(change))
			{
				item.execute(getContext(), change);
			}
		}
	}


	protected ICefValidationContext getDeterminationContext() {
		return getAssembler().getValidationContext(getValueObjContext());
	}
}
