/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.validation.lengthvaladaptor;

import com.inspur.edp.bef.api.action.validation.IValidationContext;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.api.validation.IValueObjValidationContext;
import com.inspur.edp.cef.core.i18n.I18nResourceUtil;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.i18n.framework.api.I18nContext;
import io.iec.edp.caf.i18n.framework.api.resource.IResourceLocalizer;
import io.iec.edp.caf.i18n.framework.localclient.i18nresource.I18nResourceLocalizer;
import io.iec.edp.caf.runtime.config.CefBeanUtil;

public class StringLengthValAdaptor extends LengthValAdaptor<String>
{

	private  int cefVersioin=0;

	public StringLengthValAdaptor(String propName, int length, int prec)
	{
		super(propName, length, prec);

	}

	public StringLengthValAdaptor(String propName, int length, int prec,int cefVersioin)
	{
		this(propName, length, prec);
		this.cefVersioin = cefVersioin;
	}

	@Override
	protected
	String getDisplayMessage(ICefValidationContext context) {

		String language= CAFContext.current.getLanguage();
		String exceptionCode=I18nResourceUtil.getResourceItemValue("pfcommon","cef_exception.properties","Gsp_Cef_StringLenDecVal_0001");
		String exceptionCode3=I18nResourceUtil.getResourceItemValue("pfcommon","cef_exception.properties","Gsp_Cef_LenVal_0003");
		String exceptionCode4=I18nResourceUtil.getResourceItemValue("pfcommon","cef_exception.properties","Gsp_Cef_LenVal_0001");
		String i18nMessage;
		if(context instanceof IValueObjValidationContext){

//			IUnifiedDataType dataType=(IUnifiedDataType)(((UdtValidationContext)context).getUdtContext().getValueObjDataType());

			if(((IValueObjValidationContext)context).getParentContext()==null || ((IValueObjValidationContext)context).getPropertyName()==null || "".equals(((IValueObjValidationContext)context).getPropertyName())) {

				i18nMessage=context.getPropertyI18nName(propName);//兼容之前的业务字段校验信息提示

			}
			else {
				//业务字段必填提示信息提示BE的字段名称

				i18nMessage = (((IValueObjValidationContext)context).
						getParentContext()).
						getPropertyI18nName(((IValueObjValidationContext)context).getPropertyName());

			}
		}
		else {
			//BE上的必填校验提示信息
			i18nMessage=context.getPropertyI18nName(propName);

		}
		exceptionCode=String.format(exceptionCode,i18nMessage,Integer.toString(this.length));
		return	exceptionCode;

	}

	@Override
	protected boolean isValid(String value){
		//return length == 0 || value == null || value.length() <= length;
		//电科院兼容
		if(cefVersioin==0)
		return length == 0 || length == 1 || value == null || value.length() <= length;
		else
			return length==0||value==null||value.length()<=length;
	}
}
