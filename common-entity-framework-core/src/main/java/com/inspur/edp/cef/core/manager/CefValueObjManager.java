/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.manager;

import com.fasterxml.jackson.databind.JsonNode;
import com.inspur.edp.cef.api.dataType.valueObj.ICefValueObject;
import com.inspur.edp.cef.api.determination.NestedTransmitType;
import com.inspur.edp.cef.api.manager.ICefManagerContext;
import com.inspur.edp.cef.api.manager.ICefValueObjManager;
import com.inspur.edp.cef.api.manager.serialize.CefSerializeContext;
import com.inspur.edp.cef.api.manager.serialize.NestedSerializeContext;
import com.inspur.edp.cef.api.manager.valueObj.ICefValueObjManagerContext;
import com.inspur.edp.cef.api.repository.INestedRepository;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IValueObjData;

import java.util.HashMap;

public abstract class CefValueObjManager extends CefDataTypeManager implements ICefValueObjManager {

  ///#region CreateData
  @Override
  public final ICefData createDataType() {
    IValueObjData dataType = createDataCore();
    return dataType;
  }

  protected abstract IValueObjData createDataCore();

  public abstract INestedRepository getRepository();

  @Override
  protected final ICefManagerContext _CreateMgrContext() {
    return createMgrContext();
  }

  protected abstract ICefValueObjManagerContext createMgrContext();

  //transDtm
  public abstract ICefValueObject createValueObject(IValueObjData data);


  private NestedSerializeContext nestedSerializeContext;

  private NestedSerializeContext getNestedSerializeContext() {
    if (nestedSerializeContext == null) {
      nestedSerializeContext = new NestedSerializeContext();
    }
    return nestedSerializeContext;
  }

  public void setNestedSerializeContext(NestedSerializeContext context) {
    this.nestedSerializeContext = context;
  }

  @Override
  protected CefSerializeContext getSerizlizeContext() {
    return getNestedSerializeContext();
  }

  @Override
  public NestedTransmitType getTransmitType() {
    return NestedTransmitType.ValueChanged;
  }

  @Override
  public ICefData deserialize(HashMap<String, JsonNode> nodes, ICefData data){
    throw new UnsupportedOperationException();
  }

  @Override
  public IChangeDetail deserializeChange(HashMap<String, JsonNode> nodes, IChangeDetail changeDetail){
    throw new UnsupportedOperationException();
  }
}
