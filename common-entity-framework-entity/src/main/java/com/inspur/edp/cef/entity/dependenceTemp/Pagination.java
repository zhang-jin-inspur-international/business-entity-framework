/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.dependenceTemp;

public class Pagination {
    private  int pageSize;
    private  int pageIndex;

    public Pagination()
    {}

    public Pagination(int pageSize, int pageIndex)
    {
        this.pageSize = pageSize;
        this.pageIndex = pageIndex;
    }

    public int getPageSize()
    {return pageSize;
    }
    public void setPageSize(int size)
    {
        pageSize=size;
    }

    private  int totalCount=0;
    public int getTotalCount()
    {
        return totalCount;
    }
    public void setTotalCount(int size)
    {
        totalCount=size;
    }

    private  int pageCount=0;
    public int getPageCount()
    {
        if (this.pageSize > 0)
            return (int)Math.ceil((double) this.totalCount / (double) this.pageSize);
        return this.totalCount > 0 ? 1 : 0;
    }
    public void setPageCount(int size)
    {
        pageCount=size;
    }

    public int getPageIndex()
    {
        return pageIndex;
    }
    public void setPageIndex(int size)
    {
        pageIndex=size;
    }
}
