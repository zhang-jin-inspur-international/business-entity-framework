/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.config;

public class CefExtendConfig
{
	private String privateID;
	public final String getID()
	{
		return privateID;
	}
	public final void setID(String value)
	{
		privateID = value;
	}
	private String privateSourceConfigID;
	public final String getSourceConfigID()
	{
		return privateSourceConfigID;
	}
	public final void setSourceConfigID(String value)
	{
		privateSourceConfigID = value;
	}
	private MgrConfig privateMgrConfig;
	public final MgrConfig getMgrConfig()
	{
		return privateMgrConfig;
	}
	public final void setMgrConfig(MgrConfig value)
	{
		privateMgrConfig = value;
	}
	private int privateExtendLayer;
	public final int getExtendLayer()
	{
		return privateExtendLayer;
	}
	public final void setExtendLayer(int value)
	{
		privateExtendLayer = value;
	}
	private RepositoryConfig privateRepositoryConfig;
	public final RepositoryConfig getRepositoryConfig()
	{
		return privateRepositoryConfig;
	}
	public final void setRepositoryConfig(RepositoryConfig value)
	{
		privateRepositoryConfig = value;
	}
}
