/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.accessor.entity;

import com.inspur.edp.cef.entity.accessor.base.IAccessor;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;

public abstract class RootEntityAccessor extends EntityAccessor implements IRootAccessor
{
	private static final Object itemAdded = new Object();
	@Override
  public void addItemAddedListener(OnChildAddedListener listener)
	{
		addHandler(itemAdded, listener);
	}

	@Override
  public void removeItemAddedListener(OnChildAddedListener listener)
	{
		removeHandler(itemAdded, listener);
	}

	private static final Object itemRemoved = new Object();
	@Override
  public void addItemRemovedListener(OnChildRemovedListener listener)
	{
		addHandler(itemRemoved, listener);
	}

	@Override
  public void removeItemRemovedListener(OnChildRemovedListener listener)
	{
		removeHandler(itemRemoved, listener);
	}

	protected RootEntityAccessor(IEntityData inner)
	{
		super(inner);
	}

	@Override
  public IAccessor getRoot()
{
	return this;
}

	public final void fireChildAdded(IChildAccessor addedItem, IEntityDataCollection belongCollection)
	{
    fire(itemAdded, (OnChildAddedListener listener) -> listener
        .OnChildAdded(addedItem, belongCollection, this));
	}

	public final void fireChildRemoved(IChildAccessor removedAccessor, IEntityDataCollection belongCollection)
	{
		fire(itemRemoved, (OnChildRemovedListener listener) -> listener
        .OnChildRemoved(removedAccessor, belongCollection, this));
	}
}
