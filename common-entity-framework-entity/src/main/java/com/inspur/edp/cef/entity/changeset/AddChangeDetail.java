/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.changeset;

import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IKey;

public class AddChangeDetail implements IChangeDetail, IKey
{
	public final String getID()
	{
		if (getEntityData() == null)
		{
			throw new UnsupportedOperationException();
		}
		return getEntityData().getID();
	}
	public final void setID(String value)
	{
		throw new UnsupportedOperationException();
	}

	private IEntityData privateEntityData;
	public final IEntityData getEntityData()
	{
		return privateEntityData;
	}
	public final void setEntityData(IEntityData value)
	{
		privateEntityData = value;
	}

	public AddChangeDetail()
	{
	}

	public AddChangeDetail(IEntityData data)
	{
		this();
		setEntityData(data);
	}

	public ChangeType getChangeType ()
	{return ChangeType.Added;}

	@Deprecated
	public String getDataID(){return getID();}
	@Deprecated
	public void setDataID(String value){setID(value);}


	public IChangeDetail clone() {
	return InnerUtil.cloneAddChange(this);
	}

	public final ValueObjModifyChangeDetail getNestedChange(String propName)
	{
		DataValidator.checkForEmptyString(propName, "propName");
		if (getEntityData() == null)
		{
			throw new UnsupportedOperationException();
		}

		Object changeValue = getEntityData().getValue(propName);
		return InnerUtil.buildNestedChange(changeValue);
	}
}
