/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api;

import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectRtService;
import io.iec.edp.caf.i18n.framework.api.resource.IResourceLocalizer;
import io.iec.edp.caf.runtime.config.CefBeanUtil;

public class CefRtBeanUtil {

  private static volatile IDatabaseObjectRtService dboRtService;

  public static IDatabaseObjectRtService getDboRtService() {
    if (dboRtService == null) {
      dboRtService = SpringBeanUtils.getBean(IDatabaseObjectRtService.class);
    }
    return dboRtService;
  }

  private static volatile ICefValueGetter valueGetter;

  public static ICefValueGetter getValueGetter() {
    if (valueGetter == null) {
      valueGetter = SpringBeanUtils.getBean(ICefValueGetter.class);
    }
    return valueGetter;
  }


  public static String getResourceItemValue(String currentSu, String metadataId, String key)
  {
    IResourceLocalizer localizer= CefBeanUtil.getAppCtx().getBean(IResourceLocalizer.class);
    return localizer.getString(key,metadataId,currentSu,"");
  }
}
