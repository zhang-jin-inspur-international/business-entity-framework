/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.manager;

import com.fasterxml.jackson.databind.JsonNode;
import com.inspur.edp.cef.api.dataType.valueObj.ICefValueObject;
import com.inspur.edp.cef.api.determination.NestedTransmitType;
import com.inspur.edp.cef.api.manager.serialize.NestedSerializeContext;
import com.inspur.edp.cef.api.repository.INestedRepository;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IValueObjData;

import java.util.HashMap;

public interface ICefValueObjManager extends ICefDataTypeManager {

  INestedRepository getRepository();

  //transDtm
  ICefValueObject createValueObject(IValueObjData data);

//  NestedSerializeContext getNestedSerializeContext();

  void setNestedSerializeContext(NestedSerializeContext context);

  NestedTransmitType getTransmitType();

  ICefData deserialize(HashMap<String, JsonNode> nodes, ICefData data);

  IChangeDetail deserializeChange(HashMap<String, JsonNode> nodes, IChangeDetail changeDetail);

  //region 联动计算
  //void retrieveDefaultDeterminate(IValueObjData data);

  //void afterModifyDeterminate(IValueObjData data, DataTypeModifyChangeDetail change);

  //void beforeSaveDeterminate(IValueObjData data, DataTypeModifyChangeDetail change);
  //endregion

  //region 校验规则
  //void afterModifyValidate(IValueObjData data, DataTypeModifyChangeDetail change);

  //void beforeSaveValidate(IValueObjData data, DataTypeModifyChangeDetail change);
  //endregion
}
