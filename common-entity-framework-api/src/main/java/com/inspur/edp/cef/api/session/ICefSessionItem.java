/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.session;

import com.inspur.edp.cef.api.buffer.ICefBuffer;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.api.changeListener.EntityDataChangeListener;
import com.inspur.edp.cef.api.dataType.entity.ICefRootEntity;

//按类型分离session变量
public interface ICefSessionItem {
	String getCategory();

	String getConfigId();

	//<dataId, <level, >>
	java.util.Map<String, java.util.Map<Integer, ICefBuffer>> getBuffers();

	java.util.Map<Integer, ICefBuffer> getVariableBuffers();

	EntityDataChangeListener getVarListener();

		//<dataId, <level, >>
	java.util.Map<String, java.util.Map<Integer, IChangeDetail>> getChanges();

	java.util.Map<String, ICefRootEntity> getRootEntities();
}
