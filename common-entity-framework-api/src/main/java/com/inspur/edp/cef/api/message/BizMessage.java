/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.message;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;

public class BizMessage implements IBizMessage {

  @JsonIgnore
  private String privateMessageFormat;

  @Override
  @JsonIgnore
  public final String getMessageFormat() {
    return privateMessageFormat;
  }

  @Override
  public final void setMessageFormat(String value) {
    privateMessageFormat = value;
  }

  @JsonIgnore
  private String[] privateMessageParams;

  @Override
  @JsonIgnore
  public final String[] getMessageParams() {
    return privateMessageParams;
  }

  @Override
  public final void setMessageParams(String[] value) {
    privateMessageParams = value;
  }

  private MessageLevel privateLevel = MessageLevel.forValue(0);

  @Override
  public final MessageLevel getLevel() {
    return privateLevel;
  }

  @Override
  public final void setLevel(MessageLevel value) {
    privateLevel = value;
  }

  private MessageLocation privateLocation;

  @Override
  public final MessageLocation getLocation() {
    return privateLocation;
  }

  @Override
  public final void setLocation(MessageLocation value) {
    privateLocation = value;
  }

//  private List<IBizMessage> details;
//  public List<IBizMessage> getDetailMessages() {
//    return details == null ? (details = new ArrayList<IBizMessage>()) : details;
//  }

  @JsonIgnore
  private RuntimeException privateException;

  @Override
  @JsonIgnore
  public final RuntimeException getException() {
    return privateException;
  }

  @Override
  public final void setException(RuntimeException value) {
    privateException = value;
  }

  @Override
  public String getMessage() {
    if (getMessageParams() == null || getMessageParams().length == 0) {
      return getMessageFormat();
    } else {
      return String.format(getMessageFormat(), getMessageParams());
    }
  }
//
//  public static void main(String[] args) throws JsonProcessingException {
//    BizMessage msg = new BizMessage() {{
//      setMessageFormat("%s不能为空%s");
//      setMessageParams(new String[]{"111", "2222"});
//      setTitle("title");
//      setException(new UnsupportedOperationException());
//      setLocation(new MessageLocation(){{
//        setNodeCode("nodedddd");
//        setDataIds(Arrays.asList("id1", "id2"));
//        setColumnNames(Arrays.asList("col1", "col2"));
//      }});
//    }};
//
//    System.out.println(new ObjectMapper().writeValueAsString(msg));
//
//  }
}
