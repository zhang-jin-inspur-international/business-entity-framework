/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.session;

import com.inspur.edp.cef.api.buffer.ICefBuffer;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.api.changeListener.EntityDataChangeListener;
import com.inspur.edp.cef.api.dataType.entity.ICefRootEntity;
import lombok.Getter;
import lombok.Setter;

public class CefSessionItem implements ICefSessionItem {

  private java.util.Map<String, java.util.Map<Integer, ICefBuffer>> privateBuffers = new java.util.HashMap<String, java.util.Map<Integer, ICefBuffer>>();

  @Getter
  @Setter
  private String category;

  @Getter
  @Setter
  private String configId;

  @Override
  public final java.util.Map<String, java.util.Map<Integer, ICefBuffer>> getBuffers() {
    return privateBuffers;
  }


  private java.util.Map<Integer, ICefBuffer> privateVariableBuffers = new java.util.HashMap<Integer, ICefBuffer>();

  @Override
  public final java.util.Map<Integer, ICefBuffer> getVariableBuffers() {
    return privateVariableBuffers;
  }

  private java.util.Map<String, java.util.Map<Integer, IChangeDetail>> privateChanges = new java.util.HashMap<String, java.util.Map<Integer, IChangeDetail>>();

  @Override
  public final java.util.Map<String, java.util.Map<Integer, IChangeDetail>> getChanges() {
    return privateChanges;
  }


  private java.util.Map<String, ICefRootEntity> privateRootEntities = new java.util.HashMap<String, ICefRootEntity>();
  ;

  @Override
  public final java.util.Map<String, ICefRootEntity> getRootEntities() {
    return privateRootEntities;
  }


  private EntityDataChangeListener privateVarListener = EntityDataChangeListener.createInstance();

  @Override
  public final EntityDataChangeListener getVarListener() {
    return privateVarListener;
  }
}
