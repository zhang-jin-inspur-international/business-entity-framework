/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.spi.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefDataSerItem;
import com.inspur.edp.udt.api.Manager.serialize.IAssoInfoSerializeItem;

public abstract class AbstractAssoInfoSerializeItem extends AbstractCefDataSerItem implements
    IAssoInfoSerializeItem {

  @Override
  public final void writeEntityBasicInfo(
      JsonGenerator var1, ICefData var2, SerializerProvider var3) {
    throw new RuntimeException();
  }

  @Override
  public final boolean writeModifyPropertyJson(JsonGenerator var1, String var2, Object var3,
      SerializerProvider var4) {
    throw new RuntimeException();
  }

  @Override
  public final boolean readEntityBasicInfo(JsonParser var1, DeserializationContext var2,
      ICefData var3, String var4) {
    throw new RuntimeException();
  }

}
