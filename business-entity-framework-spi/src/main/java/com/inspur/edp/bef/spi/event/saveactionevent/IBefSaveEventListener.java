/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.event.saveactionevent;

import com.inspur.edp.bef.api.event.save.AfterSaveEventArgs;
import com.inspur.edp.bef.api.event.save.BeforeSaveEventArgs;
import io.iec.edp.caf.commons.event.IEventListener;

/**
 BEF保存操作扩展事件监听接口。
 要注册Bef的保存前后扩展，需要实现该接口，里面包含保存前扩展、保存后扩展和 保存失败后扩展三个事件。

*/
public interface IBefSaveEventListener extends IEventListener
{
	/**
	 Bef保存前事件，在执行保存前，通过触发事件做业务处理（如业务流）

	 @param args 事件参数，包括BE类型、数据ID列表、变更集列表和Lcp对象。
	*/
	void beforeSave(BeforeSaveEventArgs args);

	/**
	 Bef保存后事件，在执行保存后，通过触发该事件做保存后业务处理（如业务流）

	 @param args 事件参数，包括BE类型、数据ID列表、变更集列表和Lcp对象。
	*/
	void afterSave(AfterSaveEventArgs args);
}
