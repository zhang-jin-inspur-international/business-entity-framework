/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptor;

import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.condition.RetrieveFilter;

public class AdaptorRetrieveParam {

	private String rootCode;
	private RetrieveFilter retrieveFilter;

	public AdaptorRetrieveParam() {

	}

	public AdaptorRetrieveParam(RetrieveFilter retrieveFilter) {
		this.retrieveFilter = retrieveFilter;
	}

	public AdaptorRetrieveParam(String rootCode, RetrieveFilter retrieveFilter) {
		this(retrieveFilter);
		this.rootCode = rootCode;
	}
	// endregion

	public RetrieveFilter getRetrieveFilter() {
		return retrieveFilter;
	}
	/**
	 * 过滤条件
	 *
	 * @return
	 */
	public EntityFilter getEntityfilter(String nodeCode) {
		if(retrieveFilter == null || retrieveFilter.getNodeFilters() == null) {
			return null;
		}
		if(rootCode != null && rootCode.equals(nodeCode)) {
			EntityFilter rez = retrieveFilter.getNodeFilters().get("RootNode");
			if(rez != null) {
				return rez;
			}
		}
		return retrieveFilter.getNodeFilters().get(nodeCode);
	}

	/**
	 * retrieve返回结果中包含多语相关内容
	 *
	 * @return
	 */
	public boolean isEnableMultiLanguage() {
		return retrieveFilter != null && retrieveFilter.isEnableMultiLanguage();
	}
}
