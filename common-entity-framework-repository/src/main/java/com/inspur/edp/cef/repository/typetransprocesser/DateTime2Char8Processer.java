/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.typetransprocesser;

import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.entity.condition.FilterCondition;
import com.inspur.edp.cef.spi.jsonser.abstractcefchange.BefDateSerUtil;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTime2Char8Processer implements ITypeTransProcesser
{
	private static DateTime2Char8Processer instance;
	public static DateTime2Char8Processer getInstacne()
	{
		if (instance == null)
		{
			instance = new DateTime2Char8Processer();
		}
		return instance;
	}
	private DateTime2Char8Processer()
	{

	}

	public final Object transType(FilterCondition filter, Connection db)
	{
		try
		{

			Date dateVal = BefDateSerUtil.getInstance().readDateTime(filter.getValue());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			filter.setValue(sdf.format(dateVal));
			return filter.getValue();
		}

		catch (RuntimeException e)
		{
			throw new RuntimeException("时间格式转换错误", e);
		}
	}

	public final Object transType(Object value)
	{
		if (value == null)
		{
			return null;
		}
		if(value instanceof  String)
		    return value;
		java.util.Date date = (java.util.Date)value;
		if (date == null)
		{
			throw new RuntimeException("Error Type");
		}
		if (date.compareTo(new Date(0)) <= 0)
		{
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return sdf.format( ((java.util.Date)value));
	}

	@Override
	public Object transType(Object value, boolean isNull) {
		return transType(value);
	}
}
