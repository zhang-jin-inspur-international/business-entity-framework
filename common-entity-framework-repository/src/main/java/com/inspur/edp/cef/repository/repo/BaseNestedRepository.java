/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.repo;

import com.inspur.edp.cef.api.manager.ICefDataTypeManager;
import com.inspur.edp.cef.api.repository.INestedRepository;
import com.inspur.edp.cef.api.repository.dac.INestedDac;
import com.inspur.edp.cef.api.repository.readerWriter.ICefReader;
import com.inspur.edp.cef.entity.entity.ICefData;
import java.util.HashMap;

public abstract class BaseNestedRepository implements INestedRepository {
    public BaseNestedRepository() {
    }

    private INestedDac innerGetDac() {
        INestedDac dac = getNestedDac();
        dac.setBelongRepository(this);
        return dac;
    }

    protected abstract INestedDac getNestedDac();

    public final ICefData readData(ICefReader reader) {
        return innerGetDac().readData(reader);
    }

    public Object getPersistenceValue(String colName, ICefData data) {
        return innerGetDac().getPersistenceValue(colName, data);
    }

    public Object getPersistenceValue(String colName, ICefData data, boolean isNull) {
        return innerGetDac().getPersistenceValue(colName, data, isNull);
    }

    @Override
    public HashMap<String, String> getAssosPropDBMapping(String propName) {
        return innerGetDac().getAssosPropDBMapping(propName);
    }

    public abstract ICefDataTypeManager getManager();
}
