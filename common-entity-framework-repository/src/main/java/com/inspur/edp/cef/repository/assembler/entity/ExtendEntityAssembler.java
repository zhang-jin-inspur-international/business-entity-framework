/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.assembler.entity;


/** 
 扩展持久化层组装器，
 通过标签ChildAssemblerAttribute记录扩展be中新增的子表的组装器的类，
 通过标签ExtendChildAssemblerAttribute记录基础子表的扩展的组装器的类。
 
 
 通过标签<see cref="ChildAssemblerAttribute"/>记录扩展be中新增的子表的组装器的类
 通过标签<see cref="ExtendChildAssemblerAttribute"/>记录基础子表的扩展的组装器的类
 
*/
public abstract class ExtendEntityAssembler extends EntityAssembler
{
}
