/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.session.distributed.close;

import com.inspur.edp.bef.core.session.FuncSessionUtil;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.core.context.BizContextManager;
import io.iec.edp.caf.rpc.api.service.RpcClient;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class AbnormalClosingUtil {

    private static final String CLEARCACHE = "com.inspur.edp.bef.core.session.distributed.close.SessionCacheService.clearSessionCache";

    public static AbnormalClosingUtil instance = new AbnormalClosingUtil();

    public static AbnormalClosingUtil getInstance() {
        return instance;
    }

    private static ConcurrentHashMap<String, List<String>> sessionRelationInfoInBiz = new ConcurrentHashMap<>();

    public void setSessionRelationCacheInBiz(String tokenId, String sessionId, String suCode) {
        String combinedId = FuncSessionUtil.createCombinedId(sessionId, suCode);
//        List<String> sessionIds = sessionRelationInfoInBiz.get(tokenId);
        List<String> sessionIds = sessionRelationInfoInBiz.computeIfAbsent(tokenId, key -> new ArrayList<>());
        synchronized (sessionIds) {
            if(!sessionIds.isEmpty()) {
                Iterator<String> iterator = sessionIds.iterator();
                BizContextManager bcManager = SpringBeanUtils.getBean(BizContextManager.class);
                while (iterator.hasNext()) {
                    if (bcManager.isExpired(FuncSessionUtil.getSessionIdBySplit(iterator.next()))){
                        iterator.remove();
                    }
                }
            }
            if (sessionIds.isEmpty() || !sessionIds.contains(combinedId)) {
                sessionIds.add(combinedId);
            }
        }
        if (FuncSessionUtil.redisEnabled()) {
            SessionRelationBizCacheManger.getInstance()
                .setSessionRelation(tokenId, sessionId, suCode);
        }
    }

    public void removeSessionRelationCacheInBiz(String tokenId,String sessionId,String suCode){
        String combinedId = FuncSessionUtil.createCombinedId(sessionId, suCode);
        List<String> sessionIds = sessionRelationInfoInBiz.get(tokenId);
        if (sessionIds != null) {
            synchronized (sessionIds) {
                sessionIds.remove(combinedId);
            }
        }
        if (FuncSessionUtil.redisEnabled()) {
            SessionRelationBizCacheManger.getInstance()
                .removeSessionRelation(tokenId, sessionId, suCode);
        }
    }

    public String getSessionId(String tokenId,String suCode){
        List<String> sessionIds = sessionRelationInfoInBiz.get(tokenId);
        if(sessionIds != null){
            synchronized (sessionIds) {
                if(!sessionIds.isEmpty()) {
                    String sessionId = sessionIds.get(0);
                    return FuncSessionUtil.getSessionIdBySplit(sessionId);
                }
            }
        }
        if(FuncSessionUtil.redisEnabled()) {
            sessionIds = SessionRelationBizCacheManger.getInstance().getSessionRelation(tokenId);
            if (sessionIds != null) {
                for (String sessionId : sessionIds) {
                    if (sessionId.contains(suCode)) {
                        return FuncSessionUtil.getSuCodeBySplit(sessionId);
                    }
                }
            }
        }
        return null;
    }

    @Deprecated
    public List<String> getSessionIdList(String tokenId){
        List<String> rez = sessionRelationInfoInBiz.get(tokenId);
        if(rez != null){
            return rez;
        }else if(FuncSessionUtil.redisEnabled()){
            return SessionRelationBizCacheManger.getInstance().getSessionRelation(tokenId);
        }
        return null;
    }

    public void abnormalFunctionClosing(String tokenId){
        List<String> sessionIds = clearLocalCache(tokenId);
        if(sessionIds == null && FuncSessionUtil.redisEnabled()){
            sessionIds = SessionRelationBizCacheManger.getInstance().getSessionRelation(tokenId);
            SessionRelationBizCacheManger.getInstance().evict(tokenId);
        }

        if(sessionIds==null || sessionIds.isEmpty())
            return;
        RpcClient client = SpringBeanUtils.getBean(RpcClient.class);
        LinkedHashMap<String, Object> params = new LinkedHashMap<String, Object>();
        for(String sessionId : sessionIds){
            String suCode = FuncSessionUtil.getSuCodeBySplit(sessionId);
            String realSessionId = FuncSessionUtil.getSessionIdBySplit(sessionId);
            params.clear();
            params.put("sessionId",realSessionId);
            client.invoke(Void.class,CLEARCACHE,suCode,params,null);
        }
    }

    public List<String> getValidityTokenId(){
        if(sessionRelationInfoInBiz.isEmpty())
            return null;
        List<String> tokenIds = new ArrayList<>();
        for(String key : sessionRelationInfoInBiz.keySet()){
            if(FuncSessionUtil.isTokenExpired(key)) {
                tokenIds.add(key);
            }
        }
        return tokenIds;
    }

    public List<String> clearLocalCache(String tokenId){
        return sessionRelationInfoInBiz.remove(tokenId);
    }
}
