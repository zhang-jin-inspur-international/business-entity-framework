/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.determination.builtinimpls.adaptors;

import com.inspur.edp.bef.api.action.determination.IBeforeRetrieveDtmContext;
import com.inspur.edp.cef.core.action.CompReflector;
import com.inspur.edp.cef.core.determination.builtinimpls.dtmadaptors.BaseDtmAdaptor;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

public class BefB4RetrieveDtmAdaptor extends BefBaseDtmAdaptor {

  public BefB4RetrieveDtmAdaptor(String name, Class type) {
    super(name, type, new CompReflector(type, IBeforeRetrieveDtmContext.class){
      @Override
      public Object createInstance(Object... pars) {
        return super.createInstance(pars[0]);
      }
    });
  }

  @Override
  public boolean canExecute(IChangeDetail iChangeDetail) {
    return true;
  }
}
