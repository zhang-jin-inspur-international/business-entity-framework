/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.retrieve;

import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.bef.core.scope.ScopeNodeParameter;
import com.inspur.edp.cef.entity.entity.IEntityData;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class CheckVersionScopeNodeParameter extends ScopeNodeParameter {

	//region Constructor
	public CheckVersionScopeNodeParameter(CoreBEContext context, RetrieveParam par,
			BiConsumer<CoreBEContext, IEntityData> action) {
		super(context);
		this.action = action;
		this.para = par;
	}
	//endregion Constructor

	//region 属性字段
	private RetrieveParam para;

	public RetrieveParam getParam() {
		return para;
	}

	private BiConsumer<CoreBEContext, IEntityData> action;

	@Override
	public String getParameterType() {
		return "PlatformCommon_Bef_CheckVersionScopeNodeParameter";
	}

	//endregion

	public final void executeAction(CoreBEContext beContext, IEntityData data) {
		action.accept(beContext, data);
	}

}
