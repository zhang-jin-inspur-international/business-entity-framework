/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.retrievedefault;

import com.inspur.edp.bef.api.action.assembler.IMgrActionAssembler;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.api.parameter.clone.CloneParameter;
import com.inspur.edp.bef.core.DotNetToJavaStringHelper;
import com.inspur.edp.bef.core.action.AuthorityUtil;
import com.inspur.edp.bef.core.be.BusinessEntity;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import com.inspur.edp.bef.spi.entity.builtinimpls.BefEntityResInfoImpl;
import com.inspur.edp.bef.spi.entity.columnconfig.ColumnGenerateConfigLoader;
import com.inspur.edp.bef.spi.entity.columnconfig.ColumnGenerateInfo;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.entity.resourceInfo.EntityResInfo;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;

public class CloneMgrAction extends AbstractManagerAction<IEntityData>
{

		//region Consturctor
	private String cloneDataID;
	private String dataID;
	private CloneParameter cloneParameter;
	public CloneMgrAction(IBEManagerContext managerContext, String cloneDataID, String dataID, CloneParameter cloneParameter)
	{
		super(managerContext);
		this.cloneDataID = cloneDataID;
		this.dataID = dataID;
		this.cloneParameter = cloneParameter;
	}


		//endregion


		//region Override

	private String getID() {
//		if (DotNetToJavaStringHelper.isNullOrEmpty(dataID) == false) {
//			return dataID;
//		}
		 return getColumnId();
	}

	@Override
	public final void execute()
	{
		AuthorityUtil.checkAuthority("Clone");
		FuncSessionManager.getCurrentSession().getBefContext().setCurrentOperationType("Clone");
		IBusinessEntity be = getBEManagerContext().getEntity(getID());
		be.clone(this.cloneDataID, dataID, this.cloneParameter);
		setResult(be.getBEContext().getCurrentData());
	}

	@Override
	protected final IMgrActionAssembler getMgrAssembler()
	{
		return getMgrActionAssemblerFactory().getRetrieveDefaultMgrActionAssembler(getBEManagerContext());
	}

	private EntityResInfo getEntityResInfo(){
		return  this.getBEManagerContext().getBEManager().getRootEntityResInfo();
	}

	private String getColumnId(){

		EntityResInfo resInfo=this.getEntityResInfo();
		if(!(resInfo instanceof BefEntityResInfoImpl))
			return UUID.randomUUID().toString();
		ColumnGenerateInfo columnGenerateInfo=((BefEntityResInfoImpl)resInfo).getColumnGenerateInfo();
		return ColumnGenerateConfigLoader.getId(columnGenerateInfo);
	}
		//endregion
}
