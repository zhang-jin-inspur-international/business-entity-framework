/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.retrieve;

import com.inspur.edp.bef.api.action.assembler.IBEActionAssembler;
import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.spi.action.AbstractAction;

public class AddLockAction extends AbstractAction<Boolean> {

  // region 字段属性

  // endregion

  // region Override

  @Override
  public final void execute() {
    if (!ActionUtil.getBEContext(this).isLocked()) {
      AddLockScopeNodeParameter para =
          new AddLockScopeNodeParameter((IBEContext) this.getBEContext(), rez -> setResult(rez));
      ActionUtil.getBEContext(this).addScopeParameter(para);
    }
    else
    {
      setResult(true);

    }
  }

  @Override
  protected final IBEActionAssembler getAssembler() {
    return GetAssemblerFactory().getAddLockActionAssembler(ActionUtil.getBEContext(this));
  }

  // endregion Override

  // region private
  /**
   * 加锁后回调函数
   *
   * @param beContext
   */
  private void setResult(IBEContext beContext) {
    setResult(beContext.isLocked()); // TODO:wj-不确定加锁后回调函数用途
  }

  // endregion
}
