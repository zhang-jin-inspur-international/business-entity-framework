/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.session;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.inspur.edp.bef.core.session.json.BefBizContextJsonDeSerializer;
import com.inspur.edp.bef.core.session.json.BefBizContextJsonSerializer;
import com.inspur.edp.cef.designtime.api.IGspCommonField;
import com.inspur.edp.cef.designtime.api.json.Variable.CommonVariableDeserializer;
import com.inspur.edp.cef.designtime.api.json.Variable.CommonVariableSerializer;
import io.iec.edp.caf.core.context.BizContext;
import io.iec.edp.caf.core.context.BizContextDataSerializer;
import io.iec.edp.caf.core.context.BizContextExpirationPolicy;
import io.iec.edp.caf.core.session.Session;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class BefBizContextSerializer  implements BizContextDataSerializer  {
	static ObjectMapper mapper = new ObjectMapper();
	static JavaTimeModule module = new JavaTimeModule();
	static {
		module.addDeserializer(BefBizContext.class, new BefBizContextJsonDeSerializer());
		module.addSerializer(BefBizContext.class, new BefBizContextJsonSerializer());
		mapper.registerModule(module);
	}
	@Override
	public Object serialize(Object o) {
		try {
			return mapper.writeValueAsString(o);
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public <T extends BizContext> T deserialize(Object o, Class<T> aClass) {
		if(o == null)
			return null;
		BefBizContext befBizContext = null;
		String json = o.toString();
		try {
			befBizContext = mapper.readValue(o.toString(), BefBizContext.class);
		} catch (JsonProcessingException e) {
			throw new RuntimeException("BefBizContext反序列化处理出错:" +json , e);
		}
		return (T) befBizContext;
	}
}
